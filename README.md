# Asterisk AMI Client #

Служба для создания звонков через AMI

### Для установки необходимо ###

* Развернутый образ [Elastix-2.5.0-STABLE-x86-64](http://downloads.sourceforge.net/project/elastix/Elastix%20PBX%20Appliance%20Software/2.5.0/latest/Elastix-2.5.0-STABLE-x86_64-bin-08may2015.iso?r=http%3A%2F%2Fwww.elastix.com%2Fen%2Fdownloads%2F&ts=1467890084&use_mirror=tenet)
* Знать ip-адрес машины на которой развернут Elastix (ip_elaxtix)
* Знать пароль пользователя root для Elastix (root_pasword)
> задается при развертывании образа
* Знать имя и пароль пользователя (asterisk_admin) Asterisk от имени которого будет происходить подключение(asterisk_secret)
> можно посмотреть в файле /etc/asterisk/manager.conf
* Любой ssh клиент
> например [putty](https://the.earth.li/~sgtatham/putty/latest/x86/putty.exe)
* хотябы один настроенный экстеншен 

### Установка службы ###

1. Подключаемся по ssh к Elastix командой ниже, в ответ на приглашение вводим root_password
> ssh root@ip_elastix
2. устанавливаем систему контроля версий командой 
> yum install git-core -y
3. Переходим в каталог /var/www/html командой
> cd /var/www/html
4. Клонируем в этот каталог данный репозиторий командой 
> git clone https://theevs@bitbucket.org/theevs/asterisk-client.git
5. Открываем конфигурационный файл config.php
6. В строке username после знака равенства указываем имя администратора Asterisk (asterisk_admin)
7. Аналогично указываем пароль (asterisk_secret) в строке secret

Все готово теперь нужно перейти в браузере (Mozilla, Chrome) по адресу https://asterisk_ip/asterisk-client/

Исходный код клиентской части (frontend) находится в [репозитории](https://bitbucket.org/theevs/astrisk-client-ts)

Также можно пользоваться сервисом из других приложений например командой ниже, заменив номера и указав верный ip-адрес
> curl --insecure -H "Content-Type: application/json" -X POST -d '{"fromId":201,"toId": 202}' https://192.168.130.92/asterisk-client/backend/index.php