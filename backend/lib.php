<?php

function autoloader($class) {
  $filePath = "libs/$class.php";
  if (file_exists($filePath))
  {
    require_once($filePath);
  }
}

spl_autoload_register('autoloader');

$settings = parse_ini_file('config.php', true);

function cors()
{
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');

    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
        }

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        }

        exit(0);
    }
}


if (!function_exists('json_decode')) {
  require_once 'vendor/JSON.phps';
    function json_decode($content, $assoc=false) {
        if ($assoc) {
            $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
        }
        else {
            $json = new Services_JSON;
        }
        return $json->decode($content);
    }
}

if (!function_exists('json_encode')) {
  require_once 'vendor/JSON.phps';
    function json_encode($content) {
        $json = new Services_JSON;
        return $json->encode($content);
    }
}
