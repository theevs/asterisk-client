<?php
header('Content-Type: application/json');

require_once 'call.php';
//инициализируем Класс менеджера
try {
  CallManager::init($settings);
  //Создаем объект отправленный из клиента
  $call = CallManager::fromPost();
  //вызываем метод Originate
  $call->makeCall();
  //Выводим сериализованные данные
  echo $call;
  //Отключаемся от Asterisk
  CallManager::dispose();

} catch (Exception $e) {
  http_response_code(500);
  echo json_encode(array('message' => $e->getMessage()));
}
