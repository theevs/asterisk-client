<?php
abstract class AMI_ActionMessage extends AMI_Message
{

  public function __construct($action)
  {
    parent::__construct();
    $this->setKey('Action', $action);
    $this->setKey('ActionID', microtime(true));
  }

  public function setCallerId($callerId)
  {
    return $this->setKey('CallerId', $callerId);
  }

}

 ?>
