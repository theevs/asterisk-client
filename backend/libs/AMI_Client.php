<?php
class AMI_Client
{
  static private $DEFAULT_OPTIONS = array('scheme' => 'tcp://', 'cTimeout' => 10000, 'rTimeout' => 10000);

  private $options = array();
  private $socket;
  private $lastActionID;
  private $currentProcessingMessage = '';
  private $eventFactory;

  public function __construct(array $opts)
  {
      $this->options = array_merge(self::$DEFAULT_OPTIONS, $opts);
      $this->options['port'] = intval($opts['port']);
      $this->options['cTimeout'] = intval($this->options['cTimeout']);
      $this->options['rTimeout'] = intval($this->options['rTimeout'] / 1000);

      $this->eventFactory = new AMI_EventFactory();
      $this->incomingQueue = array();
  }

  public function __get($name)
  {
    if (!isset($this->options[$name]))
    {
      return null;
    }
    return $this->options[$name];
  }

  public function open()
  {
    $errno = 0;
    $errstr = '';
    $this->socket = @fsockopen($this->host, $this->port, $errno, $errstr, $this->cTimeout);

    if ($this->socket === false) {
      throw new Exception('Error connectign to ami: ' . $errstr);
    }

    $message = new AMI_LoginActionMessage($this->username, $this->secret);

    $resp = $this->send($message);
    if (!$resp->isSuccess())
    {
      throw new Exception("Ошибка авторизации");
    }
  }

  public function close()
  {
    fclose($this->socket);
  }

  public function send(AMI_ActionMessage $message)
  {
    $messageToSend = $message->serialize();
    $length = strlen($messageToSend);
    $this->lastActionID = $message->getActionID();

    if(@fwrite($this->socket, $messageToSend) < $length) {
      throw new Exception("Could not send message");
    }

    $stop = time() + $this->rTimeout;
    while ((time() <= $stop) || ($this->rTimeout == 0)) {
      $this->process();
      $response = $this->getRelated($message);
      if ($response != false)
      {
        $this->lastActionID = false;
        return $response;
      }
      sleep(1);
    }
    throw new Exception("Read Timeout");
  }

  public function process()
  {
    $messages = $this->getMessages();

    foreach ($messages as $message) {
      $resPos = strpos($message, 'Response:');
      $evePos = strpos($message, 'Event:');
      if (($resPos !== false) && (($resPos < $evePos) || $evePos === false)) {
        $response = $this->messageToResponse($message);
        $this->incomingQueue[$response->getActionID()] = $response;
      } elseif ($evePos !== false) {
        $event = $this->messageToEvent($message);
        $response = $this->findResponse($event);

        if ($response !== false && !$response->isComplete())
        {
          $response->addEvent($event);
        }
      }
    }
  }

  private function messageToResponse($message)
  {
    $response = new AMI_ResponseMessage($message);
    $actionId = $response->getActionID();
    if (is_null($actionId))
    {
      $actionId = $this->lastActionID;
      $response->setActionID($actionId);
    }
    return $response;
  }

  private function messageToEvent($message)
  {
    return $this->eventFactory->createFromRaw($message);
  }

  private function getRelated(AMI_ActionMessage $message)
  {
    $actionId = $message->getActionID();
    if (isset($this->incomingQueue[$actionId]))
    {
      $response = $this->incomingQueue[$actionId];
      if ($response->isComplete())
      {
        unset($this->incomingQueue[$actionId]);
        return $response;
      }
    }
    return false;
  }

  private function findResponse(AMI_IncomingMessage $message)
  {
    $actionId = $message->getActionID();
    if (isset($this->incomingQueue[$actionId])) {
      return $this->incomingQueue[$actionId];
    }
    return false;
  }

  private function getMessages()
  {
    $messages = array();

    $read = @fread($this->socket, 65535);

    if ($read === false || @feof($this->socket)) {
      throw new Exception("Error reading");
    }

    $this->currentProcessingMessage .= $read;

    while (($marker = strpos($this->currentProcessingMessage, AMI_Message::EOM))) {
      $message = substr($this->currentProcessingMessage, 0, $marker);
      $this->currentProcessingMessage = substr(
        $this->currentProcessingMessage,
        $marker + strlen(AMI_Message::EOM)
      );
      $messages[] = $message;
    }

    return $messages;
  }


}

?>
