<?php
class AMI_LoginActionMessage extends AMI_ActionMessage
{

  function __construct($user, $password)
  {
    parent::__construct('Login');
      $this->setKey('Username', $user)
        ->setKey('Secret', $password);
  }
}
 ?>
