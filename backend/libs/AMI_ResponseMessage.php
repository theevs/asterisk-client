<?php
class AMI_ResponseMessage extends AMI_IncomingMessage
{
  private $events;
  private $completed;

  public function isComplete()
  {
    return $this->completed;
  }

  public function addEvent(AMI_EventMessage $event)
  {
    $this->events[] = $event;
    $this->setKey('Reason', $event->getReason());
    $this->completed = true;
  }

  public function isSuccess()
  {
    return stristr($this->getKey('Response'), 'Error') === false;
  }

  public function getMessage()
  {
    return $this->getKey('Message');
  }

  public function isQueued()
  {
    return stristr($this->getMessage(), 'queued') !== false;
  }


  public function __construct($rawContent)
  {
    parent::__construct($rawContent);
    $this->events = array();
    $this->completed = !$this->isQueued();
  }
}

 ?>
