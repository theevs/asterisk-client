<?php
abstract class AMI_IncomingMessage extends AMI_Message
{
  protected $rawContent;

  public function __construct($rawContent)
  {
    parent::__construct();
    $this->rawContent = $rawContent;
    $lines = explode(AMI_Message::EOL, $rawContent);

    foreach ($lines as $line) {
      $content = explode(':', $line);
      $name = trim($content[0]);
      $value = isset($content[1]) ? trim($content[1]) : '';
      $this->setKey($name, $value);
    }
  }

  public function getReason()
  {
    return $this->getKey('Reason');
  }

}
 ?>
