<?php
abstract class AMI_Message
{
  const EOL = "\r\n";
  const EOM = "\r\n\r\n";

  protected $keys;

  public function __construct()
  {
    $this->keys = array();
  }

  public function getKeys()
  {
    return $this->keys;
  }

  public function getKey($name)
  {
    if (!isset($this->keys[$name]))
    {
      return null;
    }
    return $this->keys[$name];
  }

  public function setKey($key, $value)
  {
    $key = (string) $key;
    $this->keys[$key] = (string)($value);
    return $this;
  }

  public function serialize()
  {
    $result = array();
    foreach ($this->getKeys() as $key => $value) {
      $result[] = "$key: $value";
    }
    $mStr = $this->finish(implode(self::EOL, $result));
    return $mStr;
  }

  protected function finish($message)
  {
    return $message . self::EOM;
  }

  public function getActionID()
  {
    return $this->getKey('ActionID');
  }
}
 ?>
