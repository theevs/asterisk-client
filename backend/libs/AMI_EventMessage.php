<?php
class AMI_EventMessage extends AMI_IncomingMessage
{
  public function getName()
  {
    return $this->getKey('Event');
  }

  public function getEventList()
  {
    return $this->getKey('EventList');
  }
  public function getResponse()
  {
    return $this->getKey('Response');
  }
  public function getReason()
  {
    return $this->getKey('Reason');
  }
}

 ?>
