<?php
class AMI_EventFactory
{
  public static function createFromRaw($message)
  {
    $eventStart = strpos($message, 'Event: ') + 7;
    $eventEnd = strpos($message, AMI_Message::EOL, $eventStart);
    if ($eventEnd === false) {
      $eventEnd = strlen($message);
    }
    $name = substr($message, $eventStart, $eventEnd - $eventStart);
    $className = "AMI_" . $name ."Event";
    if (class_exists($className, true)){
      return new $className($message);
    }
    return new AMI_EventMessage($message);
  }
}
 ?>
