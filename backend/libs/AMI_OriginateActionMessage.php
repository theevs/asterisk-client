<?php
class AMI_OriginateActionMessage extends AMI_ActionMessage
{
  public function __construct($from, $to)
  {
    parent::__construct('Originate');
    $this->setKey('Channel', "SIP/$from")
      ->setKey('Exten', $to)
      ->setKey('Async', 'true')
      ->setKey('Priority', 1);
      // ->setKey('Variable', 'SIPADDHEADER="Call-Info:\;answer-after=0"');
  }
  public function setContext($context)
  {
    return $this->setKey('Context', $context);
  }
}
 ?>
