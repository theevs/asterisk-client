<?php
require_once 'lib.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);


class CallManager
{
  //Соединение с Asterisk
  private static $client;
  //Настройки Менеджера: Отобржаемое Имя, Контекст по умолчанию
  private static $generalOptions;
  //Параметры соединения
  private static $conntectionParams;
  //Шаблон массива для json сериализации информации о звонке
  private static $json_template =  array('fromId' => '', 'toId' => '' ,'actionId' => '', 'state' => 0, 'id' => '');

  //Массив с параметрами звонка
  private $options;

  //Инициализация параметров менеджера и установка соединение
  static function init (array $settings)
  {
    self::$generalOptions = $settings['caller'];
    self::$conntectionParams = $settings['connection'];
    self::$client = new AMI_Client(self::$conntectionParams);
    self::$client->open();

  }

  //Отключение от Asterisk
  static function dispose()
  {
    self::$client->close();
  }

  function __construct($args = array())
  {
    //Объединяем шаблонный массив с входящим по ключам
    $array  = array_merge(self::$json_template, $args);
    //Добавляем не назначенные параметры
    $this->options = array_merge(self::$generalOptions, $array);

  }

  //Создаем объект из потока php://input
  static function fromPost()
  {
    $post_args = json_decode(file_get_contents('php://input'), true);
    $post_args = $post_args? $post_args : array();
    return new self($post_args);
  }
  //Получаем массив для сериализации по шаблону
  function toArray()
  {
    $json_keys = array_keys(self::$json_template);
    return array_intersect_key($this->options, array_flip($json_keys));
  }
  //Создаем сообщение для вызова метода Originate
  function originateMessage()
  {
    $originateMessage = new AMI_OriginateActionMessage($this->fromId, $this->toId);
    $originateMessage->setContext($this->context)
      ->setCallerId($this->callerId);
    $this->actionId = $originateMessage->getActionID();
    return $originateMessage;
  }

  //Отправляем сообщение, ждем три секунды для того чтоб сообщение успело обработаться
  function makeCall()
  {
    $originateMessage = $this->originateMessage();
    $res = $this->send($originateMessage);
    $this->state = intval($res->getReason());
    return $res;
  }

  //Обертка для отправки сообщений
  private function send($message)
  {
    return self::$client->send($message);
  }
//Магические методы
  function __set($name, $value)
  {
    if (array_key_exists($name, $this->options))
    {
      $this->options[$name] = $value;
    }
  }

  function __get($name)
  {
    if (array_key_exists($name, $this->options))
    {
      return $this->options[$name];
    }
    return null;
  }

  function __toString()
  {
    return json_encode($this->toArray());
  }
}

?>
